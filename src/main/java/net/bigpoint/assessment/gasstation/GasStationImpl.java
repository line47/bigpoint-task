package net.bigpoint.assessment.gasstation;

import net.bigpoint.assessment.gasstation.exceptions.GasTooExpensiveException;
import net.bigpoint.assessment.gasstation.exceptions.NotEnoughGasException;

import java.text.DecimalFormat;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Created by Talal Ahmed on 07/08/2018
 */
public class GasStationImpl implements GasStation {

    private AtomicReference<Double> revenue;
    private AtomicInteger numberOfSales;
    private AtomicInteger numberOfCancellationsNoGas;
    private AtomicInteger numberOfCancellationsTooExpensive;

    private Map<GasType, Double> gasPriceMap;
    private Map<GasType, List<GasPump>> gasPumpsMap;

    public GasStationImpl() {
        gasPriceMap = new ConcurrentHashMap<>();  // concurrent cause clients might call getter/setter in threaded environment
        gasPumpsMap = new ConcurrentHashMap<>(); // assuming there could be multiple pumps of one type
        for (GasType gasType : GasType.values()) {
            gasPumpsMap.put(gasType, new CopyOnWriteArrayList<>());
        }

        revenue = new AtomicReference<>(0.0);
        numberOfSales = new AtomicInteger(0);
        numberOfCancellationsNoGas = new AtomicInteger(0);
        numberOfCancellationsTooExpensive = new AtomicInteger(0);
    }

    @Override
    public void addGasPump(GasPump gasPump) {
        if (gasPump != null) {
            gasPumpsMap.get(gasPump.getGasType()).add(gasPump);
        }
    }

    @Override
    public Collection<GasPump> getGasPumps() {
        return clonePumps();
    }

    @Override
    public double buyGas(GasType gasType, double amountInLiters, double maxPricePerLiter)
            throws NotEnoughGasException, GasTooExpensiveException {

        checkPrice(gasType, maxPricePerLiter);

        Optional<Double> pay = tryBuy(gasType, amountInLiters, maxPricePerLiter);

        if (!pay.isPresent()) noGas();

        return pay.get();
    }

    @Override
    public double getRevenue() {
        return formatted(revenue);
    }

    @Override
    public int getNumberOfSales() {
        return numberOfSales.get();
    }

    @Override
    public int getNumberOfCancellationsNoGas() {
        return numberOfCancellationsNoGas.get();
    }

    @Override
    public int getNumberOfCancellationsTooExpensive() {
        return numberOfCancellationsTooExpensive.get();
    }

    @Override
    public double getPrice(GasType gasType) {
        return gasPriceMap.get(gasType);
    }

    @Override
    public void setPrice(GasType gasType, double price) {
        gasPriceMap.put(gasType, price);
    }

    private Optional<GasPump> filterPumps(List<GasPump> pumpList, Predicate<? super GasPump> predicate) {
        return pumpList
                .stream()
                .filter(predicate)
                .findFirst();
    }

    private List<GasPump> clonePumps() {
        List<GasPump> gasPumpClones = gasPumpsMap.values()
                .stream()
                .flatMap(Collection::stream)
                .map(item -> new GasPump(item.getGasType(), item.getRemainingAmount()))
                .collect(Collectors.toList());
        return gasPumpClones;
    }

    private void checkPrice(GasType gasType, double maxPricePerLiter) throws GasTooExpensiveException {
        Double gasPrice = gasPriceMap.get(gasType);
        if (maxPricePerLiter < gasPrice) {
            numberOfCancellationsTooExpensive.getAndIncrement();
            throw new GasTooExpensiveException();
        }
    }

    private Optional<Double> tryBuy(GasType gasType, double amountInLiters, double maxPricePerLiter) {
        List<GasPump> gasPumps = gasPumpsMap.get(gasType);
        for (GasPump gasPump : gasPumps) {
            synchronized (gasPump) {
                if (gasPump.getRemainingAmount() > amountInLiters) {
                    gasPump.pumpGas(amountInLiters);
                    double toPay = maxPricePerLiter * amountInLiters;
                    numberOfSales.getAndIncrement();
                    revenue.getAndUpdate(x -> x + toPay);
                    return Optional.of(toPay);
                }
            }
        }
        return Optional.empty();
    }

    private void noGas() throws NotEnoughGasException {
        numberOfCancellationsNoGas.getAndIncrement();
        throw new NotEnoughGasException();
    }

    private Double formatted(AtomicReference<Double> revenue) {
        return Double.valueOf(new DecimalFormat("#.##").format(revenue.get()));
    }
}
