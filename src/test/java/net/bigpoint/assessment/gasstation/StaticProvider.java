package net.bigpoint.assessment.gasstation;

import org.testng.annotations.DataProvider;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

/**
 * Created by Talal Ahmed on 07/08/2018
 */
public class StaticProvider {

    private static final int DEFAULT_CLIENT_INSTANCES = 50; // number of clients that call GasStation functions
    private static final int DEFAULT_GAS_PUMP_INSTANCES = 5; // number of gas pumps of each type

    private static final double DEFAULT_GAS_AMOUNT = 100; // default capacity each pump is initialized with
    private static final double DEFAULT_GAS_PRICE = 10; // default gas price of each pump

    static GasStation gasStation;

    static double totalRevenue;
    static int totalSales;
    static int totalCancelledOnNoGas;
    static int totalCancelledOnGasExpensive;

    @DataProvider(name = "buySuccess", parallel = true)
    public static Object[][] mockBuySuccess() {
        totalSales = 0;
        totalRevenue = 0.0;

        List<List<Object>> params = new ArrayList<>();
        IntStream.range(0, DEFAULT_CLIENT_INSTANCES)
                .forEach(c -> gasStation.getGasPumps()
                        .forEach(pump -> {
                            double liters = rand(1, DEFAULT_GAS_AMOUNT / DEFAULT_CLIENT_INSTANCES);
                            double price = rand(DEFAULT_GAS_PRICE, DEFAULT_GAS_PRICE + c);
                            totalRevenue += price * liters;
                            totalSales++;
                            params.add(Arrays.asList(pump.getGasType(), liters, price));
                        }));


        totalRevenue = Double.valueOf(new DecimalFormat("#.##").format(totalRevenue));

        return paramsAsArray(params);
    }

    @DataProvider(name = "notEnoughGas", parallel = true)
    public Object[][] mockNotEnoughGas() {
        totalCancelledOnNoGas = 0;

        List<List<Object>> params = new ArrayList<>();
        IntStream.range(0, DEFAULT_CLIENT_INSTANCES)
                .forEach(c -> gasStation.getGasPumps()
                        .forEach(pump -> {
                            double liters = rand(DEFAULT_GAS_AMOUNT + 1, DEFAULT_GAS_AMOUNT * 2);
                            double price = rand(DEFAULT_GAS_PRICE, DEFAULT_GAS_PRICE + c);
                            totalCancelledOnNoGas++;
                            params.add(Arrays.asList(pump.getGasType(), liters, price));
                        }));

        return paramsAsArray(params);
    }

    @DataProvider(name = "gasTooExpensive", parallel = true)
    public Object[][] mockGasTooExpensive() {
        totalCancelledOnGasExpensive = 0;

        List<List<Object>> params = new ArrayList<>();
        IntStream.range(0, DEFAULT_CLIENT_INSTANCES)
                .forEach(c -> gasStation.getGasPumps()
                        .forEach(pump -> {
                            double price = rand(1, DEFAULT_GAS_PRICE * 0.2);
                            double liters = rand(1, DEFAULT_GAS_AMOUNT + c);
                            totalCancelledOnGasExpensive++;
                            params.add(Arrays.asList(pump.getGasType(), liters, price));
                        }));

        return paramsAsArray(params);
    }

    static void initStation() {
        gasStation = new GasStationImpl();

        for (GasType gasType : GasType.values()) {
            IntStream.range(0, DEFAULT_GAS_PUMP_INSTANCES)
                    .forEach((x) -> {
                        gasStation.addGasPump(new GasPump(gasType, DEFAULT_GAS_AMOUNT));
                        gasStation.setPrice(gasType, DEFAULT_GAS_PRICE);
                    });
        }
    }

    private static double rand(double start, double end) {
        double random = new Random().nextDouble();
        return start + (random * (end - start));
    }

    private static Object[][] paramsAsArray(List<List<Object>> params) {
        return params.stream().map(List::toArray).toArray(Object[][]::new);
    }

    private static GasType randGasType() {
        return GasType.values()[new Random().nextInt(GasType.values().length)];
    }
}
