package net.bigpoint.assessment.gasstation;

import net.bigpoint.assessment.gasstation.exceptions.GasTooExpensiveException;
import net.bigpoint.assessment.gasstation.exceptions.NotEnoughGasException;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static net.bigpoint.assessment.gasstation.StaticProvider.*;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertThrows;

/**
 * Created by Talal Ahmed on 07/08/2018
 * #minimum_effort
 */
public class GasStationImplTest {

    @BeforeClass
    public void setup() {
        StaticProvider.initStation();
    }

    @Test(dataProvider = "buySuccess", dataProviderClass = StaticProvider.class)
    public void testBuy(GasType gasType, double liters, double price) throws NotEnoughGasException, GasTooExpensiveException {
        double toPay = gasStation.buyGas(gasType, liters, price);
        double calculated = price * liters;

        assertEquals(toPay, calculated);
    }

    @Test(dataProvider = "notEnoughGas", dataProviderClass = StaticProvider.class)
    public void testBuyIfNotEnoughGas(GasType gasType, double liters, double price) {
        assertThrows(NotEnoughGasException.class, () -> {
            double toPay = gasStation.buyGas(gasType, liters, price);
        });
    }

    @Test(dataProvider = "gasTooExpensive", dataProviderClass = StaticProvider.class)
    public void testBuyIfGasTooExpensive(GasType gasType, double liters, double price) {
        assertThrows(GasTooExpensiveException.class, () -> {
            double toPay = gasStation.buyGas(gasType, liters, price);
        });
    }

    @Test(dependsOnMethods = {"testBuyIfNotEnoughGas"})
    public void testGetNumberOfCancellationsNoGas() {
        assertEquals(totalCancelledOnNoGas, gasStation.getNumberOfCancellationsNoGas());
    }

    @Test(dependsOnMethods = {"testBuyIfGasTooExpensive"})
    public void testGetNumberOfCancellationsTooExpensive() {
        assertEquals(totalCancelledOnGasExpensive, gasStation.getNumberOfCancellationsTooExpensive());
    }

    @Test(dependsOnMethods = {"testBuy"})
    public void testGetNumberOfSales() {
        assertEquals(totalSales, gasStation.getNumberOfSales());
    }

    @Test(dependsOnMethods = {"testBuy"})
    public void testGetRevenue() {
        assertEquals(totalRevenue, gasStation.getRevenue());
    }
}